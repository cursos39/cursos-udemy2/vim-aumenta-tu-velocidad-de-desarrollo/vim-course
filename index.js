const { ayuda } = require('./helper.js')

const suma = (ns) => {
  let acumulado = 0;
  for (i = 0; i < ns.length; i++) {
    acumulado += ns[i];
  }
  ayuda()
  return acumulado;
} 

const unoAlCinco = [1, 2, 3, 4, 5];
// Un poco de texto 
const multiplicados = unoAlCinco.map(x => x * 2)
// a pares
const parejas = unoAlCinco.map(x => [x, x])

const mascotas = [
  { nombre: 'Pelusa', age: 12, tipo: 'gato' },
  { nombre: 'Puchini', edad: 12, tipo: 'perro' },
  { nombre: 'Chanchito feliz', edad: 3, tipo: 'perro' },
  { nombre: 'Pulga', edad: 10, tipo: 'perro' },
]
// edad promedio
const edades = mascotas.map(x => x.edad)
const resultado = suma(edades)
console.log(resultado / edades.length);

module.exports = {
  edades, resultado

