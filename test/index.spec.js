const { expect } = require('chai')
const { resultado, edades } = require('./../index.js')

describe('Testing the index.js file', () => {
  it('Testing the file', () => {
    const mediaEdad = resultado / edades.length
    
    expect(mediaEdad).to.be.equal(9.25)
  })
})
