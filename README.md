# VIM Aumenta tu velocidad de desarrollo 

## Índice de vídeos 

### Section 1: Introduccióon 

1. Introducción 
2. Instalación de vim o neovim 

  ```sh 
  brew install --HEAD neovim 
  ```

3. Salir de vim o neovim 

  ```sh 
  ESC ESC :q 
  ``` 
  ```sh 
  ESC ESC :q! 
  ```

4. Los modos


### Section 2: Lección 1

5. Moviendo el cursor y entre palabras.
  * `j`: Con el dedo índice de la mano derecha puesto en la letra `J`. 
  * `l`: Si te quieres mover a la izquierda, pulsar la letra `L`.
  * `h`: Si te quieres mover a la derecha, pulsar la letra `H`.
  * `k`: Si te quires mover hacia abajo, pulsar la letra `K`.
  * `j`: Si te quieres mover hacia arriba, pulsar la letra `J`.
  * `w`: En una misma línea, cambiar al principio del siguiente objeto de texto, pulsar `W`.
  * `b`: Cambiar al principio del anterior objeto de texto, pulsar `B`.

6. Insertar texto y agregarlo al final.
  * `i`: Cambiar de modo normal a modo insert, pulsar la letra `i`.
  * `a`: Cambiar de modo normal a modo insert en la siguiente letra, pulsar `a`.
  * `A`: Cambiar al modo insert al final de la línea, pulsar `A`(mayúscula).

7. Eliminar texto.
  * Eliminar un caracter desde el modo normal como si fuera SUPR: `x`. Esto no elimina texto de la línea posterior en caso de que la línea actual se haya finalizado.
  * `x`: Eliminar un carácter desde el modo normal como si fuera *SUPR*.

8. Guardar archivo.
  * Para guardar el archivo, en modo normal escribir `:w`.
  * Si queremos guardar el archivo y salir, escribir `:wq`.
  * `:w`: Guardar el archivo.
  * `:wq`: Guardar el archivo y salir.

9. Moverse entre archivos.
  * Ir a la definición de una palabra. En modo normal pulsar `gd`. (Go to Definition).
  * Para navegar a la definición de un archivo, posicionarse sobre el require, pulsar `gf`. (Go to File).
  * Para ir al anterior buffer de navegación `CTRL+o`.
  * Para ir al siguiente buffer de navegación `CTRL+i`.
  * `gd`: Ir a la definición de una palabra. (*Go to Definition*).
  * `gf`: Navegar a la definición de un archivo (* Go to File*).
  * `CTRL+o`: Ir al anterior buffer de navageación (anterior archivo).
  * `CTRL+i`: Ir al siguiente buffer de navegación (siguiente archivo).

### Section 3: Comandos y pegar

10. Comandos para eliminar, undo y redo.
  * Comando para eliminar un objeto de texto, pulsar `dw`. (Delete Word).
  * Comando para deshacer cambios en modo normal, pulsar `u`. (Undo).
  * Comando para rehacer los cambios que habíamos deshecho, pulsar `CTRL+r`. (Redo).
  * Comando para eliminar desde la posición del cursor hasta el final de una línea `d $`. Normalmente $ es `SHIFT+4`.
  * `dw`: Elimina un objeto de texto.
  * `u`: Deshacer cambios en modo normal (*undo*).
  * `CTRL+r`: Rehacer los cambios que habíamos deshecho (*redo*).
  * `d $`: Eliminar desde la posición del cursor hasta el final de la línea.

11. Operadores y movimientos.
  Para poder, por ejemplo eliminar combinando movimientos simplemente pulsaremos `d ${comando_de_movimiento}`.
  Por ejemplo, si pulsamos en modo normal `d b` estaremos borrando desde el cursor hasta el anterior objeto de texto.
  También podremos borrar desde el cursor hasta el siguiente objeto de texto pulsando `d e`. Si queremos borrar desde
  el cursor hasta que acabe el objeto de texto, pulsaremos `d w`.
  Podemos multiplicar el uso de operadores o movimientos usando los números. Por ejemplo, si quisiéramos movernos
  tres objetos hacia adelante, pulsaremos `b 3`.
  * `d ${comando_demovimiento}`: Eliminar combinando comando de movimiento (p.e. `d b`: borramos desde el cursor hasta el anterior
  objeto de texto).
  * `d e`: borramos desde el cursor hasta el siguiente objeto de texto.
  * `d w`: borramos desde el cursor hasta que acabe el objeto de texto.
  * `b 3 `: nos movemos tres obejtos hacia adelante.

12. Eliminando líneas, pegar y reordenar listas.
  Para eliminar una línea entera, pulsamos `dd`. Esta selección se quedará en el clipboard. Si luego la queremos
  insertar debajo de una línea en la que estemos posicionados, pulsaremos `p` (Paste). En caso de querer insertarla
  en la línea de arriba, pulsaremos `P`.
  * `dd`: eliminamos una línea entera.
  * `p`: pegamos debajo de la línea en la que estemos posicionados. (*Paste*).
  * `P`: pegamos en la línea por encima del cursor. (*Paste* línea de arriba).

### Section 4: Reemplazar y cambiar

13. Operador de cambio.
  * `cw`: Para cambiar una palabra: `cw` (Change Word).
  * `ciw`: Para cambiar una palabra entera independientemente de dónde esté el cursor: `ciw`. (Change I Word).

## Section 5: Saltos de cursor

14. Saltando a líneas, comienzo y final del archivo y buscar.
  * `gg`: Ir al principio del archivo.
  * `G`: Saltar al final del archivo.
  * `16G`: Ir a la línea 16 (podemos poner cualqueir número).
  * `/edad + ENTER`: Buscar las coincidencias de *edad* desde el cursor hacia adelante.
  * `?edad + ENTER`: Buscar las coincidencias de *edad* desde el cursor hacia abajo.
  * `n`: Siguiente ocurrencia de una búsqueda. (*next*).
  * `N`: Anterior ocurrencia de una búsqueda.
15. Saltando al paréntesis correspondiente y reemplazando cadenas.
  * `%`: Moverse entre paréntesis, brackets, corchetes (`()`, `{}`, `[]`).
  * `0`: Moverse al comienzo de una línea.
  * `$`: Moverse al final de una línea.
  * `:s/the/de`: modificar `the` por `de`en una línea una vez.
  * `:s/the/de/g`: modificar `the` por `de` en una línea todas las veces.
  * `:%s/the/de/g`: modificar `the` por `de` en todo el fichero.
  * `:%s/the/de/gc`: modificar `the` por `de` en todo el fichero preguntándonos en cada una de las ocurrencias.


### Section 6: Más comandos.

16. Abrir nueva línea, reemplazar, copiar y pegar.

### Section 7: Configurando vim o neovim.

17. Configurando vim y neovim.
  Configurar fichero `~/.config/nvim/init.vim`
  Incluido fichero de configuración `.vimrc` en el proyecto.

### Section 8 Plugins.

18. Instalando gestor de extensiones.
19. Instalando temas.
20. Easymotion.
21. Nerdtree.
22. Navegando entre archivos abiertos.
23. Creando atajos personalizados.

### Section 9: Clase extra.

24. Clase extra.
  

## Comandos

Instalación

```sh
brew install --HEAD neovim
```
